unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Unit2, ExtCtrls, ExtCtrlsX, Menus, TSettings;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
    scrlbx1: TScrollBox;
    btn1: TButton;
    trycn1: TTrayIcon;
    pm1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    procedure btn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure setOnChangedForAllItems;
    procedure onHotKeyChanged(sender:TObject);
    procedure onRemoveBtnClicked(sender:TObject);
    procedure clearItems;
  protected
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  items:TList;
  settings:TSettingsManager;

implementation

{$R *.dfm}


procedure TForm1.btn1Click(Sender: TObject);
var
  item:TChangeItems;
begin
  item := TChangeItems.Create(Self);
  item.Parent := scrlbx1;
  item.Top := items.Count * 25;
  items.Add(item);
  settings.saveItems(items);
  self.setOnChangedForAllItems;
end;

procedure TForm1.onHotKeyChanged(sender:TObject);
begin
  settings.saveItems(items);
end;

procedure TForm1.clearItems;
var
  i:integer;
  item:TChangeItems;
begin
  while self.scrlbx1.ControlCount > 0 do begin
    self.scrlbx1.RemoveControl(self.scrlbx1.Controls[0]);
  end;
end;

procedure  TForm1.onRemoveBtnClicked(sender:TObject);
var i:integer;
    item:TChangeItems;
    windowItems:TList;
begin
  item := (sender as TButton).parent as TChangeItems;
  for i:=0 to items.Count - 1 do begin
    if item = items[i] then begin
      items.Remove(item);
      settings.saveItems(items);
      self.clearItems;
      items := settings.loadItems(scrlbx1);
      self.setOnChangedForAllItems;
      exit;
    end;
  end;
end;

procedure TForm1.setOnChangedForAllItems;
var
  i:integer;
  item:TChangeItems;
begin
  for i:= 0 to items.Count - 1 do begin
    item:=items[i];
    item.hk1.OnChange := onHotKeyChanged;
    item.hk2.OnChange := onHotKeyChanged;
    item.removeBtn.OnClick := self.onRemoveBtnClicked;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
    settings := TSettingsManager.Create;
    items := settings.loadItems(scrlbx1);
    self.setOnChangedForAllItems;
end;

end.
