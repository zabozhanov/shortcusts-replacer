unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls;

type
   TChangeItems = class(TWinControl)

public

 hk1:THotKey;
  hk2:THotKey;
  removeBtn:tbutton;

  constructor Create(AOwner:TComponent); override;

end;

implementation

procedure Register;
begin
  RegisterComponents('TChangeItems', [TChangeItems]);
end;

constructor TChangeItems.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  hk1 := THotKey.Create(self);
  hk2 := THotKey.Create(self);

  self.Width := 300;
  self.Height := 18;

  hk1.Parent := self;
  hk2.Parent := Self;
  hk2.Left := hk1.Width + 5;

  removeBtn := TButton.Create(Self);
  removeBtn.Parent := self;
  removeBtn.Width := 50;
  removeBtn.Height := 18;
  removeBtn.Top := 0;
  removeBtn.Left := 250;
  removeBtn.Caption := '�������';
end;

end.
