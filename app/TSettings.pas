unit TSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Unit2, ExtCtrls, ExtCtrlsX, Menus;

type TSettingsManager = class (TObject)
public
  function saveItems(items:TList):String;
  function loadItems(AOwner:TWinControl):TList;
end;

implementation

function hotkeytostring(hk:THotKey):String;
const
  VK2_SHIFT   = 32;
  VK2_CONTROL = 64;
  VK2_ALT     = 128;
  VK2_WIN     = 256;

var res:String;
  item:TChangeItems;
  hotkey:dword;
  chars:integer;
  modif:integer;
begin
  hotkey := hk.HotKey;

  chars := byte(hotkey);
  modif := hotkey shr 8;

  if (modif and 32) > 1 then res:=res + '1' else res:=res + '0';  //shift
  if (modif and 64) > 1  then res:=res + '1' else res:=res + '0'; //control.
  if (modif and 128) > 1  then res:=res + '1' else res:=res + '0'; //alt

  res:=res + inttostr(chars);
  result:=res;
end;

function TSettingsManager.saveItems(items:TList):String;
  var i:integer;
  res:TStringList;
    current:String;
    item:TChangeItems;
begin
  res:=TStringList.Create;
for i:=0 to items.Count - 1 do begin
  item := items[i];
  current:=IntToStr(item.hk1.HotKey) + ' ' + IntToStr(item.hk2.HotKey);
  res.Add(current);
end;
res.SaveToFile(GetCurrentDir + '\keys.txt');
result:=res.Text;
end;

function hotkeyFromString(str:String):TShortCut;
begin

showmessage(str);
end;

function TSettingsManager.loadItems(AOwner:TWinControl):TList;
var
  items:TStringList;
  i:integer;
  item:TChangeItems;
  hotkey:TStringList;
  res:TList;
  settingsPath:String;
begin
  items := TStringList.Create;
  res:=TList.Create;
  result := res;

  settingsPath := GetCurrentDir + '\keys.txt';
  if FileExists(settingsPath) = false then exit;
  items.LoadFromFile(settingsPath);
  for i:=0 to items.Count - 1 do begin
    item := TChangeItems.Create(AOwner);
    item.Parent := AOwner;
    item.Top := 25 * i;
    hotkey := TStringList.Create;
    hotkey.Delimiter := ' ';
    hotkey.DelimitedText := items[i];

    item.hk1.HotKey := StrToInt(hotkey.Strings[0]);
    item.hk2.HotKey := StrToInt(hotkey.Strings[1]);

    res.Add(item);
  end;
end;

end.
